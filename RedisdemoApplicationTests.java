package com.yongyou;

import com.yongyou.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;
/**
  *测试类
  */
@SpringBootTest
class RedisdemoApplicationTests {
    @Autowired
    RedisUtil redisUtil;

    @Test
    void setKey() {
        boolean flag= redisUtil.set("xiaoshuo", "择天记");
		System.out.println("-----------------------------------");
    }

    @Test
    void getKey() {
        Object xiaoshuo = redisUtil.get("xiaoshuo");
        System.out.println(xiaoshuo);
    }

    @Test
    void setHash(){
        Map map = new HashMap();
        map.put("1","斗破");
        map.put("2","神墓");
        redisUtil.hmset("爱吃西红柿",map);
    }

    @Test
    void getHash(){
        Map<Object, Object> map = redisUtil.hmget("爱吃西红柿");
        System.out.println(map.get("1"));
    }

    @Test
    void hasKey(){
        boolean flage = redisUtil.hasKey("爱吃西红柿");
        System.out.println(flage);
    }

    @Test
    void delete(){
        redisUtil.del("爱吃西红柿");
    }
}